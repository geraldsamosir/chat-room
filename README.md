# chat-room

## Date of submission
30 November 2023

## Instructions to run assignment locally
here the step by step to use this chat app https://drive.google.com/file/d/1KikXYAUdSkyt82JjO0jYWcrg7gPL6nCJ/view?usp=sharing

process:
1. you must have docker and docker-compose
2. install and run all service in root project docker-compose up -d
3. access web in port 8080
4. join room with name and room name
5. do chat

## Time spent
12 hours


## Assumptions
the requirement just simple chat to comunicate in a room chat

## production Plan
1. make unit testing di ensure the services work as well
2. based on socket.io documentation this websocket cannot handle more than 1000 (https://socket.io/docs/v4/performance-tuning/#at-the-os-level) user concurent
and fot this aproach is still ok but if need more must adjusted with mongodb adapter and replication (https://socket.io/docs/v4/mongo-adapter/)
3. security for now just checking by email and room , no duplication user in same room