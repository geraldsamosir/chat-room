'use strict'

const roomsSchema = require('../model/room')

class RoomServices {
   async getChatsByRoomId (roomId) {
      const chats = await roomsSchema.find({ room_id: roomId }).sort({ created: 1 })
      return chats
   }
   
   async createChat (roomId, username, message) {
      const create = new roomsSchema({ 
        room_id: roomId,
        username,
        message
      })

      await create.save()

      return true
   }
}

module.exports = new RoomServices()