'use strict'

const userSchema = require('../model/user')

class UserService {

   async getUserActive (roomId, username) {
     return await userSchema.findOne({
        username,
        last_room_id: roomId, 
        online_status: true 
    })
   }

   async setUserStatus (roomId, username, onlineStatus) {
     await userSchema.updateOne(
        { username },
        {
          $set: {
            online_status: onlineStatus,
            last_room_id: roomId
          }
        },
        { upsert: true }
     )


     return true
   }
}

module.exports = new UserService()