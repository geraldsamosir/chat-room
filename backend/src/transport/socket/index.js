const Handler = require('./handler')

const handlerSocket = (socket) => {
   const socketHanlder = new Handler(socket)
   
   // handlers
   socket.on('join', socketHanlder.join)
   socket.on('sendMessage', socketHanlder.sendMessage)
   socket.on('leave', socketHanlder.leave)
}

module.exports = socket => handlerSocket(socket)