'use strict'

const RoomServices = require('../../../service/room.service')
const UserService = require('../../../service/user.service')
const SEND_MESSAGE = 'message'
const INIT_MESSAGE = 'init_message'

class SocketHandler {
   constructor (socket) {
     this.socket = socket
     this.join = this.join.bind(this)
     this.sendMessage = this.sendMessage.bind(this)
     this.leave = this.leave.bind(this)
   }

   /**
    * join room
    * 
    * @param {object} payload 
    * @param {string} payload.roomId
    * @param {boolean} payload.onJoinRoom
    * 
    * 
    * @returns {Promise<object>}
    */
   async join ({roomId, onJoinRoom, username}) {
     const chats = await RoomServices.getChatsByRoomId(roomId)
     
     const userActive = await UserService.getUserActive(roomId, username)

     if (userActive) {
       return this.socket.emit(INIT_MESSAGE, {
         success: false,
         message: `user ${username} has join in room ${roomId}`
       })
     }

     if (!onJoinRoom) this.socket.join(roomId)

     await UserService.setUserStatus(roomId, username, true)


     return this.socket.nsp.in(roomId)
        .emit(INIT_MESSAGE, {
           success: true,
           data: {
            chats,
            roomId 
          }
        })
   }

   /**
    * leave room
    * 
    * @param {Object} payload
    * @param {string} payload.roomId
    * @param {boolean} payload.onJoinRoom
    * 
    *  
    * @returns {Promise<boolean>}
    */
   async leave ({ roomId, username, onJoinRoom }) {
     if(onJoinRoom) {
        console.log('user leave room ', roomId)
        this.socket.leave(roomId)
     }

     await UserService.setUserStatus(roomId, username, false)

     return true
   }

   /**
    * send message
    * 
    * this is send message and broadcast to spesifict room that user joinning
    * 
    * @param {Object} payload
    * @param {string} payload.message
    * @param {string} payload.roomId
    * @param {string} payload.username
    * 
    *  
    * @returns {Promise<object>}
    */
   async sendMessage ({message, roomId, username}) {
    const userActive = await UserService.getUserActive(roomId, username)

    if (!userActive) {
      return this.socket.nsp.in(roomId)
        .emit(SEND_MESSAGE, {
          success: false,
          message: 'user status inactive cannot send message'
        })
    }

    
     await RoomServices.createChat(roomId, username, message)

     return this.socket.nsp.in(roomId)
      .emit(SEND_MESSAGE, {
         success: true,
         data: {
          message,
          chat: true,
          username,
          room_id: roomId
        } 
      })
   }
}

module.exports = SocketHandler

