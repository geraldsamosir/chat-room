const mongoose = require('mongoose')

const { Schema } = mongoose;

const userSchema = new Schema({
  username: String,
  online_status: Boolean,
  last_room_id: String,
  created: { type: Date, default: Date.now }
})


module.exports = mongoose.model("user_status", userSchema);