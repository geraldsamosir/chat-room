const mongoose = require('mongoose')

const { Schema } = mongoose;

const roomsSchema = new Schema({
  username: String,
  room_id: String,
  message: String,
  created: { type: Date, default: Date.now }
})


module.exports = mongoose.model("room_chat", roomsSchema);