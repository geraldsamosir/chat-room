'use strict'

require('dotenv').config()
const express = require("express")
const { createServer } = require("http")
const { Server } = require("socket.io")
const { createClient } = require('redis')
const { createAdapter } = require('@socket.io/redis-adapter')
const handlerSocket = require('./src/transport/socket')
const mongoose = require('mongoose')
const mongoUrl = process.env.DB_CONNECTION_STR

console.log('momngo', mongoUrl)

const app = express();
const httpServer = createServer(app)

const io = new Server(httpServer,{
    allowEIO3: true,
    cors: {
      origin: true,
      methods: ['GET', 'POST'],
      credentials: true
    }
})

mongoose.connect(mongoUrl)
  .then(() => console.log('mongodb connection succesful'))
  .catch(((err) => {
    if (err.message.code === 'ETIMEDOUT') {
      // Attempting to re-establish database connection
      mongoose.connect(mongoUrl);
    } else {
      // Error while attempting to connect to database
      throw err;
    }
  }));

io.on("connection", (socket) => {
  socket.emit('WELCOME', 'connected')
  handlerSocket(socket)
})

httpServer.listen(3000, ()=> {
   console.log('server run on port 3000') 
})

