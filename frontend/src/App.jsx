import { useEffect, useState, useRef } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import { App as AppLayout, Page } from 'konsta/react';
import './App.css'
import { io } from "socket.io-client"

import {
  Outlet
} from "react-router-dom";

const MAX_SCREEN_SIZE = 900

function App() {
  const socket = useRef()
  const [screenWidth, setScreenWidtht] = useState(0)

  
  useEffect(() => {
    setScreenWidtht(window.innerWidth)
    socket.current = io("http://localhost:3000")

    socket.current.on("connect", () => {
      console.log("connected to server");
    })

    socket.current.on("connect_error", (err) => {
      console.log(`connect_error due to ${err.message}`);
    })

  },[socket])

  return (
    <AppLayout theme='ios'>
      <Page 
      className='max-w-4xl mx-auto lg:max-w-md ' 
      style={{
        margin: screenWidth > MAX_SCREEN_SIZE ? '0 35%': '0 auto', 
      }}>
         <Outlet context={{ socket: socket.current }} />
      </Page>
    </AppLayout>
  )
}

export default App
