import { Formik, Field, Form } from 'formik'
import { useOutletContext, useLocation } from 'react-router-dom'

export default function FormChat ({ }) {
    const  { socket } = useOutletContext()
    const { chats, roomId, username } = JSON.parse(localStorage.getItem('roomState'))

    return (
        <div className='bottom-0 fixed w-full xl:w-1/3 lg:w-1/3'>
        <Formik
        initialValues={{
          message: ''
        }}
        onSubmit={(value, {resetForm}) => {
          socket.emit('sendMessage', { message: value.message, roomId, username })
          resetForm()
        }}>
        
         <Form>
            <Field type='text' name='message' className="rounded-full p-2 w-3/4" placeholder="Message"/>
            <button type='submit' className='bg-green-500 p-2 w-1/4 lg:w-1/5 xl:w-1/5 rounded-full'>send</button>
         </Form>
        </Formik>
        </div>
    )
}