import { Formik, Field, Form } from 'formik'
import { useEffect, useState } from 'react';
import { useNavigate, useOutletContext } from 'react-router-dom'
import * as Yup from 'yup'

const joinRoomSchema = Yup.object().shape({
  roomId: Yup.string().required('RoomId is required'),
  username: Yup.string().required('Username is required')
})

export default function Home() {
  const navigate = useNavigate()
  const [errorLoginMessage, setErrorLoginMessage] = useState(null)
  const { socket } = useOutletContext()
  const { roomValue, setRoomValue } = useOutletContext()

  return (
    <div className='container'>
      <div className="text-center w-full mt-2">
        <h1 className='text-lg text-bold font-medium'>Join Chatroom</h1>  
      </div> 
      <Formik
        initialValues={{
          roomId: '',
          username: ''
        }}
        validationSchema={joinRoomSchema}
        onSubmit={(value) => {
          const { roomId, username } = value
          socket.emit('join', { username , roomId} )

          const eventListener = (payload)=> {

            if(payload.data?.chat) return 

            if (!payload.success) {
              setErrorLoginMessage(payload.message)
              setTimeout(()=>{
                window.location.reload(false)
              }, 2000)
              return
            }

            navigate(`/room/${roomId}`)

            localStorage.setItem('roomState', JSON.stringify({
              chats: [...payload.data.chats], 
              roomId: roomId,
              username
            }))
            localStorage.setItem("username", username)
          } 

          socket.on('init_message', eventListener)

        }}
        >
        {({ errors, isSubmitting }) => (
            <Form className="flex w-full flex-col p-5 space-y-5">   
            <Field type='text' name='username' className="rounded p-2" placeholder="Username"/>
            {errors.username && (
             <p className='text-red-500'>{errors.username}</p>
            )}
            <Field  type='text' name='roomId' className="rounded p-2" placeholder="RoomID"/>
            {errors.roomId && (
             <p className='text-red-500'>{errors.roomId}</p>
            )}
            <button type="submit" className='bg-green-500 btn btn-success rounded-full p-2 px-4 m5-10 text-white' disabled={isSubmitting}>JOIN</button>
            { 
              !!errorLoginMessage && (<p className='text-red-500 text-center'>{errorLoginMessage}</p>) 
            }
          </Form>
        )}
      </Formik>
    </div>
  )  
}