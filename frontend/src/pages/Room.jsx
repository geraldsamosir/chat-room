import { useEffect, useState } from 'react'
import { useOutletContext, useLocation, useNavigate } from 'react-router-dom'
import FormChat from '../component/formChat'


export default function Room() {
    const navigate = useNavigate()
    const { socket } = useOutletContext()
    const { chats, roomId, username } = JSON.parse(localStorage.getItem('roomState'))
    const myUsername = localStorage.getItem('username')


    const [listChat, setListChat] = useState(null)

    useEffect(()=>{
      if (!socket) return
      
      if (!listChat) setListChat([].concat(chats))

      const eventListener = (data) => {
        console.log('data dari server', data)
        setListChat((prev)=> [...prev, data.data])
      } 

      socket.on('message', eventListener)
      return () => socket.off('message', eventListener);
    }, [socket])

    

    return (
      <div className='container'>
        <div className='w-full mt-2 mb-2'>
            <center>
              <h1 className='text-lg text-center'>{roomId}</h1>
            </center>
            <a onClick={()=>{
              socket.emit('leave', {roomId, username, onJoinRoom: true })
              localStorage.clear()
              navigate('/')

            }}>Exit</a>
        </div>
        <div className='flex flex-col space-y-5 mb-15'>
        {
          listChat && listChat.map((chat, index)=> {
            return (
              <div key={index} className={`flex  ${ myUsername === chat.username ? 'justify-end mr-5': 'justify-start ml-5' }`}>
                <div>
                  <>{chat.username}</> 
                  <br/>
                  <div className={myUsername === chat.username  ?'p-5 bg-green-500 rounded-lg	text-[#E8E8E8]': 'p-5 bg-[#F6F6F6] rounded-lg'}>
                    {chat.message}
                  </div>
                </div>
              </div>
            )
          })
        }
        </div>
        <FormChat/>
      </div>
    )  
  }